#ifndef _EINK_DRIVER
#define _EINK_DRIVER

#include <Arduino.h>

int taskCount = 0;
int taskStatus[14];
String tasks[14];


void setTaskCount(int ct)
{
    if (ct < 14)
    {
        taskCount = ct;
    }
}

void setTaskStatus(int i, int status)
{
    if (i < 13)
    {
        taskStatus[i] = status;
    }
}

void setTask(int i, String task)
{
    if (i < 13)
    {
        tasks[i] = task;
    }
}

void getTask(int i)
{
    return tasks[i];
}

void getTaskStatus(int i)
{
    return taskStatus[i];
}

void getTaskNumber()
{
    return taskCount;
}

void

// TODO: FIGURE OUT EINK STUFF
void updateEink()
{
    Serial.println("UPDATE EINK");
    for (int i = 0; i < taskCount; i++)
    {
        Serial.println(tasks[i]);
    }
}

#endif
