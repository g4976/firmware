#include "EEPROM.h"
#include <WebServer.h>
// #include <ArduinoOTA.h>

WebServer server(80);

#include "functionHeaders.h"
#include "WiFiManager.h"
#include "Firebase.h"
#include "LED.h"
#include "UI.h"

#define UPDATE_TIME_SEND_DATA (60*1000)
#define UPDATE_TIME_UI 1

unsigned long sendDataPrevMillis = 0;
unsigned long uiPrevMS;

void setup() {
    pinMode(2, OUTPUT);
    pinMode(15,INPUT); //for resetting WiFi creds
    pinMode(36,INPUT); //for resetting WiFi creds
    
    EEPROM.begin(400);
    Serial.begin(115200);
    if(!CheckWIFICreds()){
        Serial.println("No WIFI credentials stored in memory. Loading form...");
        digitalWrite(2,HIGH);
        while(loadWIFICredsForm());
    }
    else
    {
          if(!connectWiFi(getSSID(), getPW()))
          {
              wipeEEPROM();
              ESP.restart();
          }

        //   // setup OTA
        //   ArduinoOTA
        //   .onStart([]() {
        //     String type;
        //     if (ArduinoOTA.getCommand() == U_FLASH)
        //       type = "sketch";
        //     else // U_SPIFFS
        //       type = "filesystem";
      
        //     // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
        //     Serial.println("Start updating " + type);
        //   })
        //   .onEnd([]() {
        //     Serial.println("\nEnd");
        //   })
        //   .onProgress([](unsigned int progress, unsigned int total) {
        //     Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
        //   })
        //   .onError([](ota_error_t error) {
        //     Serial.printf("Error[%u]: ", error);
        //     if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
        //     else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
        //     else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
        //     else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
        //     else if (error == OTA_END_ERROR) Serial.println("End Failed");
        //   });
      
        // ArduinoOTA.begin();
        // // END OTA
        
        firebaseSetup();
        UI_init();
        LED_init();
        
        digitalWrite(2,LOW);
    }
}

void loop() {
    // // Handle OTA
    // ArduinoOTA.handle();
  
    if(digitalRead(36) == HIGH){
        Serial.println("Wiping WiFi credentials from memory...");
        wipeEEPROM();
        while(loadWIFICredsForm());
    }
    if(firebaseReady() && (millis() - sendDataPrevMillis > UPDATE_TIME_SEND_DATA || sendDataPrevMillis == 0))
    {
        sendDataPrevMillis = millis();
        // firebaseSetFloat(0.01 + random(0,100), "test/float");
        // firebaseSetInt(42069, "test/int");
        // firebaseSetString("test", "test/string");
        firebaseQuery();
    }

    readRotaryEncoder();
}
