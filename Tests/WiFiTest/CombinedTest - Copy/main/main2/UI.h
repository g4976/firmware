#ifndef _UI
#define _UI

#include <Arduino.h>
#include "taskManager.h"
#include "defines.h"

// Rotary Encoder Inputs
#define CLK 27
#define DT 32
#define SW 33

int counter = 0;
int currentStateCLK;
int lastStateCLK;
String currentDir ="";
unsigned long lastButtonPress = 0;

void IRAM_ATTR encoderInterrupt() {
    // If the DT state is different than the CLK state then
    // the encoder is rotating CCW so decrement
    if (digitalRead(DT) != currentStateCLK)
    {
        counter --;
        currentDir ="CCW";
        decrementSelectedTask();
    } 
    else 
    {
        // Encoder is rotating CW so increment
        counter ++;
        currentDir ="CW";
        incrementSelectedTask();
    }

//    Serial.print("Direction: ");
//    Serial.print(currentDir);
//    Serial.print(" | Counter: ");
//    Serial.println(counter);
}

void UI_init()
{
    // Set encoder pins as inputs
    pinMode(CLK,INPUT);
    pinMode(DT,INPUT);
    pinMode(SW, INPUT_PULLUP);
    
    attachInterrupt(CLK, encoderInterrupt, RISING);

    // Read the initial state of CLK
//    lastStateCLK = digitalRead(CLK);
}

void readRotaryEncoder()
{
    // // Read the current state of CLK
    // currentStateCLK = digitalRead(CLK);

    // // If last and current state of CLK are different, then pulse occurred
    // // React to only 1 state change to avoid double count
    // if (currentStateCLK != lastStateCLK  && currentStateCLK == 1){

    //     // If the DT state is different than the CLK state then
    //     // the encoder is rotating CCW so decrement
    //     if (digitalRead(DT) != currentStateCLK)
    //     {
    //         counter --;
    //         currentDir ="CCW";
    //         decrementSelectedTask();
    //     } 
    //     else 
    //     {
    //         // Encoder is rotating CW so increment
    //         counter ++;
    //         currentDir ="CW";
    //         incrementSelectedTask();
    //     }

    //     Serial.print("Direction: ");
    //     Serial.print(currentDir);
    //     Serial.print(" | Counter: ");
    //     Serial.println(counter);
    // }

    // // Remember last CLK state
    // lastStateCLK = currentStateCLK;

    // Read the button state
    int btnState = digitalRead(SW);

    //If we detect LOW signal, button is pressed
    if (btnState == LOW) {
        //if 50ms have passed since last LOW pulse, it means that the
        //button has been pressed, released and pressed again
        if (millis() - lastButtonPress > 50) {
            Serial.println("Button pressed!");
            changeTaskState();
        }

        // Remember last button press event
        lastButtonPress = millis();
    }
}

#endif
