#ifndef _TASK_MANAGER
#define _TASK_MANAGER

#include <Arduino.h>
#include "UI.h"
#include "LED.h"

int taskCount = 0;
int selectedTask = 0;
int taskStatus[MAX_TASKS];
String tasks[MAX_TASKS];


void setTaskCount(int ct)
{
    if (ct < MAX_TASKS)
    {
        taskCount = ct;
    }
}

void setTaskStatus(int i, int status)
{
    if (i < (MAX_TASKS-1))
    {
        // switch(status)
        // {
        //     case 0:
        //         taskStatus[i] = TASK_STATUS_NOT_STARTED;
        //     case 1:
        //         taskStatus[i] = TASK_STATUS_IN_PROGRESS;
        //     case 2:
        //         taskStatus[i] = TASK_STATUS_COMPLETED;
        //     default:
        //         taskStatus[i] = TASK_STATUS_COUNT;
        // } 
        taskStatus[i] = status;
    }
}

void setTask(int i, String task)
{
    if (i < (MAX_TASKS-1))
    {
        tasks[i] = task;
    }
}

String getTask(int i)
{
    return tasks[i];
}

int getTaskStatus(int i)
{
    return taskStatus[i];
}

int getTaskNumber()
{
    return taskCount;
}

void decrementSelectedTask()
{
    updateTaskStateColor(selectedTask, false, true);
    selectedTask = (selectedTask == 0) ? taskCount-1 : (selectedTask - 1);
    Serial.println(selectedTask);
    updateTaskStateColor(selectedTask, true, true);
}

void incrementSelectedTask()
{
    updateTaskStateColor(selectedTask, false, true);
    selectedTask = (selectedTask + 1) >= taskCount ? 0 : (selectedTask + 1);
    Serial.println(selectedTask);
    updateTaskStateColor(selectedTask, true, true);
}

int getSelectedTask()
{
    return selectedTask;
}

void changeTaskState()
{
    taskStatus[selectedTask] = (taskStatus[selectedTask] == 2) ? 2 : (taskStatus[selectedTask]+1);
    firebaseChangeTaskState(selectedTask, taskStatus[selectedTask]);
    updateTaskStateColor(selectedTask, true, true);
}

#endif
