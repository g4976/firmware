// taskManager
// typedef enum
// {
//     TASK_STATUS_NOT_STARTED = 0,
//     TASK_STATUS_IN_PROGRESS = 1,
//     TASK_STATUS_COMPLETED = 2,
//     TASK_STATUS_COUNT = 3
// } task_status_E;

void setTaskCount(int ct);

void setTaskStatus(int i, int status);

void setTask(int i, String task);

String getTask(int i);

int getTaskStatus(int i);

int getTaskNumber();

void decrementSelectedTask();

void incrementSelectedTask();

int getSelectedTask();

void changeTaskState();


// Firebase
bool firebaseReady();

void firebaseSetFloat(float f, String loc);

void firebaseSetInt(int i, String loc);

void firebaseSetString(String s, String loc);

void firebaseSetBool(bool b, String loc);

// USED FOR TESTING
void firebaseAddTask(String task);

void firebaseChangeTaskState(int taskNumber, int taskStatus);

void firebaseInitDevice();

void firebaseSetup();

// TODO: Save Tasks to eeprom
bool firebaseQuery();