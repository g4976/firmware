// BASED OFF OF https://randomnerdtutorials.com/esp32-firebase-realtime-database/
#ifndef _FIREBASE
#define _FIREBASE

#define ID "da6d"

#include <Arduino.h>
#include "taskManager.h"
#if defined(ESP32)
  #include <WiFi.h>
#elif defined(ESP8266)
  #include <ESP8266WiFi.h>
#endif

#include <Firebase_ESP_Client.h>

//Provide the token generation process info.
#include "addons/TokenHelper.h"
//Provide the RTDB payload printing info and other helper functions.
#include "addons/RTDBHelper.h"

// Insert Firebase project API Key
#define API_KEY "AIzaSyCoE7DeNWo_i3STRw2vD-Auz2CNz8S4e34"

// Insert RTDB URLefine the RTDB URL */
#define DATABASE_URL "https://esp32-firebase-test-78ff0-default-rtdb.firebaseio.com/" 

String deviceIDTemp = "/";
const String deviceID = deviceIDTemp + DEVICE_ID;
    
//Define Firebase Data object
FirebaseData fbdo;

FirebaseAuth auth;
FirebaseConfig config;

bool signupOK = false;

int taskNumber = 0;

bool firebaseReady()
{
    return (Firebase.ready() && signupOK);
}

void firebaseSetFloat(float f, String loc)
{
    if (Firebase.RTDB.setFloat(&fbdo, loc, f)){
      Serial.println("PASSED");
      Serial.println("PATH: " + fbdo.dataPath());
      Serial.println("TYPE: " + fbdo.dataType());
    }
    else {
      Serial.println("FAILED");
      Serial.println("REASON: " + fbdo.errorReason());
    }
}

void firebaseSetInt(int i, String loc)
{
    if (Firebase.RTDB.setInt(&fbdo, loc, i)){
      Serial.println("PASSED");
      Serial.println("PATH: " + fbdo.dataPath());
      Serial.println("TYPE: " + fbdo.dataType());
    }
    else {
      Serial.println("FAILED");
      Serial.println("REASON: " + fbdo.errorReason());
    }
}

void firebaseSetString(String s, String loc)
{
    if (Firebase.RTDB.setString(&fbdo, loc, s)){
      Serial.println("PASSED");
      Serial.println("PATH: " + fbdo.dataPath());
      Serial.println("TYPE: " + fbdo.dataType());
    }
    else {
      Serial.println("FAILED");
      Serial.println("REASON: " + fbdo.errorReason());
    }
}

void firebaseSetBool(bool b, String loc)
{
    if (Firebase.RTDB.setBool(&fbdo, loc, b)){
      Serial.println("PASSED");
      Serial.println("PATH: " + fbdo.dataPath());
      Serial.println("TYPE: " + fbdo.dataType());
    }
    else {
      Serial.println("FAILED");
      Serial.println("REASON: " + fbdo.errorReason());
    }
}

// USED FOR TESTING
void firebaseAddTask(String task)
{
    Firebase.RTDB.getInt(&fbdo, (deviceID + "/taskCount"));
    taskNumber = fbdo.intData();

    String taskLocation = deviceID + "/task" + taskNumber;

    firebaseSetString(task, taskLocation + "/taskName");
    firebaseSetInt(0, taskLocation + "/taskStatus");
    firebaseSetInt(++taskNumber, deviceID + "/taskCount");
}

void firebaseChangeTaskState(int taskNumber, int taskStatus)
{
    String taskLocation = deviceID + "/task" + taskNumber;
    firebaseSetInt(taskStatus, taskLocation + "/taskStatus");
}

void firebaseInitDevice()
{
    // Device location string
    firebaseSetBool(true, (deviceID + "/update"));
    Firebase.RTDB.getBool(&fbdo, (deviceID + "/registered"));
    Serial.println(fbdo.boolData());
    if (fbdo.boolData() == false)
    {
        Serial.println(fbdo.boolData());
        firebaseSetBool(true, (deviceID + "/registered"));
        firebaseSetInt(0, (deviceID + "/taskCount"));
    }
}

void firebaseSetup()
{
    /* Assign the api key (required) */
    config.api_key = API_KEY;

    /* Assign the RTDB URL (required) */
    config.database_url = DATABASE_URL;

    /* Sign up */
    if (Firebase.signUp(&config, &auth, "", "")){
        Serial.println("ok");
        signupOK = true;
    }
    else{
        Serial.printf("%s\n", config.signer.signupError.message.c_str());
    }

    /* Assign the callback function for the long running token generation task */
    config.token_status_callback = tokenStatusCallback; //see addons/TokenHelper.h
    
    Firebase.begin(&config, &auth);
    Firebase.reconnectWiFi(true);

    firebaseInitDevice();
    

    // FOR TESTING
    // firebaseAddTask("test");
}

// TODO: Save Tasks to eeprom
bool firebaseQuery()
{
    if (signupOK)
    {
        Firebase.RTDB.getBool(&fbdo, (deviceID + "/update"));
        if (fbdo.boolData())
        {
            Firebase.RTDB.getInt(&fbdo, (deviceID + "/taskCount"));
            taskNumber = fbdo.intData();

            taskNumber = taskNumber < 14 ? taskNumber : 14;
            setTaskCount(taskNumber);

            for (int i = 0; i < taskNumber; i++)
            {
                String taskLocation = deviceID + "/task" + i;

                Firebase.RTDB.getString(&fbdo, (taskLocation + "/taskName"));
                String task = fbdo.stringData();
                setTask(i, task);

                Firebase.RTDB.getInt(&fbdo, (taskLocation + "/taskStatus"));
                int status = fbdo.intData();
                setTaskStatus(i, status);
            }
            firebaseSetBool(false, (deviceID + "/update"));
//            updateEink();
            setLEDcolors();
            
        }
    }
}


#endif
