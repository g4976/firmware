#ifndef _LED
#define _LED

#include <Arduino.h>
#include <Adafruit_NeoPixel.h>
#include "taskManager.h"
#include "defines.h"

#define LED_PIN 13

Adafruit_NeoPixel pixels(7, LED_PIN, NEO_GRBW + NEO_KHZ800);
//Adafruit_NeoPixel pixels(MAX_TASKS, LED_PIN, NEO_RGBW);

void LED_init()
{
    pixels.begin();
    pixels.show();
}

void updateTaskStateColor(int taskNumber, bool selected)
{
    int addValue = selected ? 90 : 0; //Make selected task brighter
    switch(getTaskStatus(taskNumber))
    {
        case 2:
            Serial.println("2");
            Serial.println(taskNumber);
            pixels.setPixelColor(taskNumber, pixels.Color(0, 20+addValue, 0));
            // pixels.setPixelColor(taskNumber, pixels.Color(0, 40, 0));
            break;
        case 1:
            Serial.println("1");
            Serial.println(taskNumber);
            pixels.setPixelColor(taskNumber, pixels.Color(15+2*addValue/3, 5+addValue/3, 0));
            // pixels.setPixelColor(taskNumber, pixels.Color(50, 20, 0));
            break;
        case 0:
            Serial.println("0");
            Serial.println(taskNumber);
            pixels.setPixelColor(taskNumber, pixels.Color(20+addValue, 0, 0));
            // pixels.setPixelColor(taskNumber, pixels.Color(100+addValue, 0, 0));
            break;
        default:
            // TODO: check if there's function to turn pixels off
            pixels.setPixelColor(taskNumber, pixels.Color(100, 100, 100));
            Serial.println("default");
    }
    pixels.show();
    Serial.println("UpdateColor");
}

// todo: add 3 bottom LEDs
void setLEDcolors()
{
    pixels.clear();
    
   for (int i = 0; i < getTaskNumber(); i++)
   {
        bool selected = (getSelectedTask() == i);
        updateTaskStateColor(i, selected);
   }
}





#endif
