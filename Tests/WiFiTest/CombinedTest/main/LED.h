#ifndef _LED
#define _LED

#include <Arduino.h>
#include <FastLED.h>
#include "taskManager.h"
#include "defines.h"

#define LED_PIN 13
#define NUM_LEDS 14
CRGB leds[NUM_LEDS];

void LED_init()
{
    FastLED.addLeds<SK6812, LED_PIN, RGB>(leds, NUM_LEDS);
}

void updateTaskStateColor(int taskNumber, bool selected, bool update)
{
    int divValue = selected ? 1 : 2; //Make selected task brighter
    switch(getTaskStatus(taskNumber))
    {
        case 2:
            Serial.println("2");
            Serial.println(taskNumber);
            leds[taskNumber] = (CRGB::Green)/divValue;
            break;
        case 1:
            Serial.println("1");
            Serial.println(taskNumber);
            leds[taskNumber] = (CRGB::Yellow)/divValue;
            break;
        case 0:
            Serial.println("0");
            Serial.println(taskNumber);
            leds[taskNumber] = (CRGB::Red)/divValue;
            break;
        default:
            // TODO: check if there's function to turn pixels off
            
            Serial.println("default");
    }
    if (update)
    {
        FastLED.show();
    }
    Serial.println("UpdateColor");
}

// todo: add 3 bottom LEDs
void setLEDcolors()
{   
   for (int i = 0; i < getTaskNumber(); i++)
   {
        bool selected = (getSelectedTask() == i);
        updateTaskStateColor(i, selected, false);
   }
   FastLED.show();
}





#endif
