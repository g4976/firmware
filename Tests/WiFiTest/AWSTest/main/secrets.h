#include <pgmspace.h>

#define SECRET
#define THINGNAME ""

const char WIFI_SSID[] = "2.4 3085CLK-U2";
const char WIFI_PASSWORD[] = "CXNK0049CCEC";
const char AWS_IOT_ENDPOINT[] = "xxxxx.amazonaws.com";

// Amazon Root CA 1
static const char AWS_CERT_CA[] PROGMEM = R"EOF(
-----BEGIN CERTIFICATE-----
MIIDQTCCAimgAwIBAgITBmyfz5m/jAo54vB4ikPmljZbyjANBgkqhkiG9w0BAQsF
ADA5MQswCQYDVQQGEwJVUzEPMA0GA1UEChMGQW1hem9uMRkwFwYDVQQDExBBbWF6
b24gUm9vdCBDQSAxMB4XDTE1MDUyNjAwMDAwMFoXDTM4MDExNzAwMDAwMFowOTEL
MAkGA1UEBhMCVVMxDzANBgNVBAoTBkFtYXpvbjEZMBcGA1UEAxMQQW1hem9uIFJv
b3QgQ0EgMTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALJ4gHHKeNXj
ca9HgFB0fW7Y14h29Jlo91ghYPl0hAEvrAIthtOgQ3pOsqTQNroBvo3bSMgHFzZM
9O6II8c+6zf1tRn4SWiw3te5djgdYZ6k/oI2peVKVuRF4fn9tBb6dNqcmzU5L/qw
IFAGbHrQgLKm+a/sRxmPUDgH3KKHOVj4utWp+UhnMJbulHheb4mjUcAwhmahRWa6
VOujw5H5SNz/0egwLX0tdHA114gk957EWW67c4cX8jJGKLhD+rcdqsq08p8kDi1L
93FcXmn/6pUCyziKrlA4b9v7LWIbxcceVOF34GfID5yHI9Y/QCB/IIDEgEw+OyQm
jgSubJrIqg0CAwEAAaNCMEAwDwYDVR0TAQH/BAUwAwEB/zAOBgNVHQ8BAf8EBAMC
AYYwHQYDVR0OBBYEFIQYzIU07LwMlJQuCFmcx7IQTgoIMA0GCSqGSIb3DQEBCwUA
A4IBAQCY8jdaQZChGsV2USggNiMOruYou6r4lK5IpDB/G/wkjUu0yKGX9rbxenDI
U5PMCCjjmCXPI6T53iHTfIUJrU6adTrCC2qJeHZERxhlbI1Bjjt/msv0tadQ1wUs
N+gDS63pYaACbvXy8MWy7Vu33PqUXHeeE6V/Uq2V8viTO96LXFvKWlJbYK8U90vv
o/ufQJVtMVT8QtPHRh8jrdkPSHCa2XV4cdFyQzR1bldZwgJcJmApzyMZFo6IQ6XU
5MsI+yMRQ+hDKXJioaldXgjUkK642M4UwtBV8ob2xJNDd2ZhwLnoQdeXeGADbkpy
rqXRfboQnoZsG4q5WTP468SQvvG5
-----END CERTIFICATE-----
)EOF";

// Device Certificate
static const char AWS_CERT_CRT[] PROGMEM = R"KEY(
-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEA4+va6YxqsMYgB8kH39F/Tos+2gglkSjJS5tRpcFpB3E4j8Sp
66mSUvraGOTvGAI6RFk+k2GYJc+/V0PpMEWOWK4oE0zeRo52BUe2YNz+v4Z9Vtc2
rtIMAFVcrHSe40r2ntFrkKfIPICuDgtX8POBMz3icNU96WQdkuHJIiFvv8OCDEu9
bR9f6aiqs+Pxjla1qbrcfh4xsVoEPb43EqGBN7rivxuuh8bHyBjyzC9uT0hWOylx
RJSf+wGxjLYMshF2WQJVuZ7Vs1uqLfbD76xDwyqEGjwBeIC4pzVm8yXkcswMXBTR
9AQzR5XCtjL4BoBvki5IOJzue2VjRQeK+XHLFwIDAQABAoIBAQDE8Qbe6bNC/l+s
+ntwXcDoaI+trH5xmNGfIB2D4cTUZnPeXqwI8yR5yUJtYlxq3Xnll0vn49upSILo
b8juKhwe6pW8X55tTyoR0+52cl85tYdWY25fzyYJn7kNV1P3yM2jXAfbHe96wCIm
Bo09qdtJIL8qikgOBLE9OxYmBMhf5HkICJx6ENFnb9XU4zF+MLGBQDTRgzSllEAd
3gfAjQcjW2W214EL5c2DS+xJkL3X7O2I3aw5nFDe3zcjn/7y+sbdmQEFFqJzvGyM
0iGmQ2wSmgWxCCVV3SlobEvkVl8VVizx23df3e1P8WTWyC1yaRX5Sw+s8uKJ1lzb
bV89ToaxAoGBAPv846yt0E3NNDmB0y6OrQboKkgSrBq2ePS29xfi3Z8yLkqWo0DN
QNBzuFu8RFStWvJRNqX35ajLPYaNedxYfr35ihrZoscywMcJh/NbOYPo1OF7AN2n
DLdfdOxw9LbXvJ8/dvBzhC9T19aYUDP8cckciKuof5P5/Bur9E3xnoufAoGBAOeM
3qmUFMCRBM7XlaKxPysdb0xkuATd4S8gE7U5ceODAih6k/pdQP9JFlcbNyBpFe3u
J/z9QM8FDgWUjmBRkqO4jLgkTDgLpIvu7YkaIMwqwajPfoXpaQzcC02Ojg2UBTsa
q8WTjaF+LwQ3LcRSSSnvvZNgfwB+/j7emONyPg2JAoGACFAVarLpNkfYuzTvNI/8
YUfzbSFAkDkI0HnguOcBDKlpIYjKpe6ffsDyigQx9OErKOdI+DyvVNwvkWrFZGF+
/OFoju4zEoM4HNjRpVOTGCjsLOwT4TwLjVkW0F4BlK6VSGOdlt0YtT781dV8f9qK
HmpUj+UlIEqU6IzJqHCaHF0CgYAV3Os5P84l0IuPrDNoxSuduo3DvM7TRs/4TyMN
PXtPbBNMFybZwOxeLlx5pQwzdzim2pHUBWWVJnkIddprpcgexVIkIOlBcXf77ndn
NpxaKr9WXSEpK7zLhgtbK1amWY+U4RPO9/urf+qYj002+D4IQ6a2v6m+Nj9zWDk/
YI4TYQKBgQDws1e2b7/io5/QTppWrCDGufbPq4zYwXbNn/rLpaeiq8Awj13j7EfR
AGddfU0G29SAt+Y+MW2/e6YbpJPxqcmiNsA9GlWc0ylRHAbZBg6m2H/7Cwunw2wj
APBIGzXjCyjMWUeUG4tDLzumf4Yd5p15mQ7AUkGwXBPn4dU+rXd30g==
-----END RSA PRIVATE KEY-----
)KEY";

// Device Private Key
static const char AWS_CERT_PRIVATE[] PROGMEM = R"KEY(
-----BEGIN CERTIFICATE-----
MIIDWTCCAkGgAwIBAgIUDtcc6ybt2kNFvsajM7tv7VY5lo8wDQYJKoZIhvcNAQEL
BQAwTTFLMEkGA1UECwxCQW1hem9uIFdlYiBTZXJ2aWNlcyBPPUFtYXpvbi5jb20g
SW5jLiBMPVNlYXR0bGUgU1Q9V2FzaGluZ3RvbiBDPVVTMB4XDTIyMDQwMjAyMzk0
MloXDTQ5MTIzMTIzNTk1OVowHjEcMBoGA1UEAwwTQVdTIElvVCBDZXJ0aWZpY2F0
ZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOPr2umMarDGIAfJB9/R
f06LPtoIJZEoyUubUaXBaQdxOI/EqeupklL62hjk7xgCOkRZPpNhmCXPv1dD6TBF
jliuKBNM3kaOdgVHtmDc/r+GfVbXNq7SDABVXKx0nuNK9p7Ra5CnyDyArg4LV/Dz
gTM94nDVPelkHZLhySIhb7/DggxLvW0fX+moqrPj8Y5Wtam63H4eMbFaBD2+NxKh
gTe64r8brofGx8gY8swvbk9IVjspcUSUn/sBsYy2DLIRdlkCVbme1bNbqi32w++s
Q8MqhBo8AXiAuKc1ZvMl5HLMDFwU0fQEM0eVwrYy+AaAb5IuSDic7ntlY0UHivlx
yxcCAwEAAaNgMF4wHwYDVR0jBBgwFoAU4oo7OkYA1YNMzFB90KC2CJ+1lb8wHQYD
VR0OBBYEFGrWZGhGu7Erj81h4rCEeRGjn2R2MAwGA1UdEwEB/wQCMAAwDgYDVR0P
AQH/BAQDAgeAMA0GCSqGSIb3DQEBCwUAA4IBAQBhVqaa2aI8dDel4xBCs/pfd0y0
ldV/soMO6BZeermfL7ulxpwGLA98AaUBfUNeKfU4k+qhEJSs+IGFNXW1MQ1jRaqB
sigPJ/Y4BJyQy7jVYbeZo0VyIzehNy1h/n6FDxAiHqtAOTLbnZh4Ojtket271CCF
JYyaWzYk2APzwLX8igONUM/2+2WEwC9JibzVrA4tFVJTdsY+qMfHcfvMr7NZCJxl
bQhlBgzNMmwxUJ+awXpBlIHusPRPeB0CQHEEGPDMolbS58nGac5bTQc+uCFE3/FQ
YyAjaYW2WRDnX2MCzceX6gvTmiKqFE+R43oWn4vtvuxviTgsRIMe9dX1oO44
-----END CERTIFICATE-----
)KEY";
