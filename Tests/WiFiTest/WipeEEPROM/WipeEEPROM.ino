#include "EEPROM.h"

void setup() {
  // put your setup code here, to run once:
  EEPROM.begin(400);
  wipeEEPROM();
}

void loop() {
  // put your main code here, to run repeatedly:

}

void wipeEEPROM(){
  for(int i=0;i<400;i++){
    EEPROM.writeByte(i,0);
  }
  EEPROM.commit();
}
