import os

# delete main.cpp.o file so it always rebuilds with new time
main_path = os.path.join(os.getcwd(), ".pio", "build", "esp32doit-devkit-v1", "src", "ota.cpp.o")
print(main_path)
if os.path.exists(main_path):
  os.remove(main_path)
else:
  print("The file does not exist")