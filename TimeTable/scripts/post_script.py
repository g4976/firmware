import os
import shutil
Import("env", "projenv")



def test(source, target, env):
    main_path = os.path.join(os.getcwd(), ".pio", "build", "esp32doit-devkit-v1", "firmware.bin")
    copy_path = os.path.join(os.getcwd(), "firmware.bin")

    shutil.copy(main_path, copy_path)

env.AddPostAction("$BUILD_DIR/firmware.bin", test)