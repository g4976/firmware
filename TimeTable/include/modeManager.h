#ifndef _MODE_MANAGER
#define _MODE_MANAGER

#include <Arduino.h>
#include <iostream>

int getCurrentModeIndex();

void setNextModeActive();

void setModeDefault();

void changeModeState();

int numLEDsActive(int mode, float sensorData, bool celsius);

int getTempGoodMinLED(bool celsius);

int getTempGoodMaxLED(bool celsius);

int getHumGoodMinLED();

int getHumGoodMaxLED();

int getAirQualGoodLED();

int getAirQualNotGoodLED();

#endif