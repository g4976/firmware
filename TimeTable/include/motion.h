#ifndef _MOTION
#define _MOTION

#include <Arduino.h>

// Rotary Encoder Inputs
#define MOTION_PIN 34

#define SIT_TIME_WARNING (10*60*1000) // can be longer, set to 10 mins for demo purposes

void IRAM_ATTR motionRisingEdge();
void IRAM_ATTR motionFallingEdge();

void motion_init();

void checkSitTime();

#endif
