#ifndef _UI
#define _UI

#include <Arduino.h>

// Rotary Encoder Inputs
#define CLK 27
#define DT 35
#define SW 33
#define BUTTON 17

#define LED_TIMEOUT (5*1000)

// void IRAM_ATTR encoderInterrupt();

void UI_init();

void readRotaryEncoder();

void manageModeButton();

#endif
