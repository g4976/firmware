#ifndef _OTA
#define _OTA
// based off of https://medium.com/@mdwdotla/over-the-air-arduino-firmware-updates-using-firebase-part-2-1438139e4540
#include "Arduino.h"

void pollOTA();

void startFirmwareUpdate(String firmwareVersion);
void updateFirmware(String firmwareUrl);


#endif