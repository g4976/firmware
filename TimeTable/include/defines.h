#define DEVICE_ID "da6d"
#define MAX_TASKS 7

// Time
#define UNIX_HOUR 3600
#define UNIX_DAY 86400
#define UNIX_WEEK 604800
#define UNIX_MONTH 2629743
