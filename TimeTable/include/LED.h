#ifndef _LED
#define _LED

#include "defines.h"

#define LED_PIN 32
#define NUM_STATUS_LEDS 3
#define NUM_LEDS (MAX_TASKS+NUM_STATUS_LEDS)

#define DIM 40
#define BRIGHT 120



int taskNumberToIndex(int taskNumber);

void LED_init();

void updateTaskStateColor(int taskNumber, bool selected, bool update);

void updateAirQualColors(int led, int goodLed, int notGoodLed);

void updateHumColors(int led, int goodLedMin, int goodLedMax);

void updateTempColors(int led, int goodLedMin, int goodLedMax);

// todo: add 3 bottom LEDs
void setLEDcolors();

void brightenSelectedLED(bool bright);

void sitTimeWarning();

void updateModeStateColor(int mode);

void setModeLEDcolors(float sensorData, bool celsius);

void turnOffTaskLED(int taskNumber);

void turnOffLEDcolors();

#endif
