#ifndef _WIFI_MANAGER
#define _WIFI_MANAGER

#include <Arduino.h>

/*
 * Function to handle unknown URLs
 */

#define TIMEOUT_MS (10 * 1000)

bool connectWiFi(String s, String p);
 
void handleNotFound();

/*
 * Function for writing WiFi creds to EEPROM
 * Returns: true if save successful, false if unsuccessful
 */
bool writeToMemory(String ssid, String pass);


/*
 * Function for handling form
 */
void handleSubmit();

/*
 * Function for home page
 */
void handleRoot();

/*
 * Function for loading form
 * Returns: false if no WiFi creds in EEPROM
 */
bool loadWIFICredsForm();

/*
 * Function checking WiFi creds in memory 
 * Returns: true if not empty, false if empty
 */
bool CheckWIFICreds();

String getSSID();

String getPW();

void setupEEPROM();

void wipeEEPROM();


#endif
