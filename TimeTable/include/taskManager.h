#ifndef _TASK_MANAGER
#define _TASK_MANAGER

#include <Arduino.h>
#include <iostream>
#include <ctime>
#include "time.h"

void setTaskCount(int ct);

void setTaskNum(int i, int taskNum);

void setTaskStatus(int i, int status);

void setTaskActive(int i, int active);

void setTask(int i, String task);

// void setTask(int i, Task task);

void setDueDateFull(int i, String dueDate);

void setDueDate(int i, String dueDate);

void setDueTimeStamp(int i, time_t timestamp);

// Task getTask(int i);

int getTaskStatus(int i);

int getTaskNumber();

int getTaskActive(int i);

String getDueDateFull(int i);

String getDueDate(int i);

time_t getdueTimeStamp(int i);

String getTaskTitle(int i);

int getTaskNum(int i);

time_t getTaskTimeTillCompletion(int i);

void decrementSelectedTask();

void incrementSelectedTask();

int getSelectedTask();

void changeTaskState();

void setSelectedTask(int task);

void printTask(int i);

#endif
