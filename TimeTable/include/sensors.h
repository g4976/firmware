#ifndef _SENSORS
#define _SENSORS

#define NUM_T_H_MEASUREMENTS 3
#define NUM_VOC_MEASUREMENTS 3

bool getFOrC();

void sensor_init();

void read_sensors();

float get_humidity();

float get_temperature();

int get_aqi();

#endif
