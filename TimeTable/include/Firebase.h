// BASED OFF OF https://randomnerdtutorials.com/esp32-firebase-realtime-database/
#ifndef _FIREBASE
#define _FIREBASE

#include <Arduino.h>

// Insert Firebase project API Key
#define API_KEY "AIzaSyCoE7DeNWo_i3STRw2vD-Auz2CNz8S4e34"

// Insert RTDB URLefine the RTDB URL */
#define DATABASE_URL "https://esp32-firebase-test-78ff0-default-rtdb.firebaseio.com/" 

#define USER_EMAIL "bx3@illinois.edu"
#define USER_PASSWORD "password"

String getDeviceID();

bool firebaseReady();

void firebaseSetFloat(float f, String loc);

void firebaseSetInt(int i, String loc);

void firebaseSetString(String s, String loc);

void firebaseSetBool(bool b, String loc);

// USED FOR TESTING
void firebaseAddTask(String task);

void firebaseChangeTaskState(int taskNumber, int taskStatus);

void firebaseInitDevice();

void firebaseSetup();

// TODO: Save Tasks to eeprom
void firebaseQuery();

#endif
