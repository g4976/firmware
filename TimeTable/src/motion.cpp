#include "motion.h"
#include "LED.h"
#include "defines.h"

static int totalTimeSitting = 0;
static int sitDownTime = 0;
static bool currentlySitting = false;

// motion detected
void IRAM_ATTR motionRisingEdge()
{
    sitDownTime = millis();
    currentlySitting = true;
}

// motion stopped
void IRAM_ATTR motionFallingEdge()
{
    totalTimeSitting = totalTimeSitting + (millis() - sitDownTime);
    currentlySitting = false;
}

void motion_init()
{
    pinMode(MOTION_PIN, INPUT);
    attachInterrupt(MOTION_PIN, motionRisingEdge, RISING);
    attachInterrupt(MOTION_PIN, motionFallingEdge, FALLING);
}

void checkSitTime()
{
    if (currentlySitting && (millis() - sitDownTime) < SIT_TIME_WARNING)
    {
        sitTimeWarning();
    }
}
