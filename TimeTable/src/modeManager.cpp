#include "UI.h"
#include "LED.h"
#include "Firebase.h"
#include "defines.h"
#include "taskManager.h"

int mode[4];
#define TEMP_MIN_C 15.0
#define TEMP_MIN_GOOD_C 20.0
#define TEMP_MAX_GOOD_C 24.0
#define TEMP_MAX_C 30.0
#define LED_INTERVAL_TEMP_C (TEMP_MAX_C - TEMP_MIN_C) / float(MAX_TASKS)
#define TEMP_MIN_F 59.0
#define TEMP_MIN_GOOD_F 68.0
#define TEMP_MAX_GOOD_F 75.0
#define TEMP_MAX_F 86.0
#define LED_INTERVAL_TEMP_F (TEMP_MAX_F - TEMP_MIN_F) / float(MAX_TASKS)
#define HUM_MIN 0
#define HUM_MIN_GOOD 30.0
#define HUM_MAX_GOOD 70.0
#define HUM_MAX 100.0
#define LED_INTERVAL_HUM (HUM_MAX - HUM_MIN) / float(MAX_TASKS)
#define AIR_QUAL_MIN 0.0
#define AIR_QUAL_GOOD 100.0
#define AIR_QUAL_NOT_GOOD 200.0
#define AIR_QUAL_MAX 500.0
#define LED_INTERVAL_AIR_QUAL  (AIR_QUAL_MAX - AIR_QUAL_MIN) / float(MAX_TASKS)
// int taskStatus[MAX_TASKS];
// String tasks[MAX_TASKS];

int getCurrentModeIndex() {
    for (int i = 0; i < 4; i++) {
        if (mode[i] == 1) {
            return i;
        }
    }
    return 0;
}

void setNextModeActive() {
    int currentActiveMode = getCurrentModeIndex();
    if (currentActiveMode == 3) { 
        mode[currentActiveMode] = 0;
        mode[0] = 1;
    } else {
        mode[currentActiveMode] = 0;
        mode[currentActiveMode + 1] = 1;
    }
}

void setModeDefault() {
    mode[0] = 1;
    mode[1] = 0;
    mode[2] = 0;
    mode[3] = 0;
    updateModeStateColor(0);
}

void changeModeState() {
    turnOffLEDcolors();
    setNextModeActive();
    Serial.print("CURRENT INDEX: ");
    Serial.println(getCurrentModeIndex());
    updateModeStateColor(getCurrentModeIndex());
}

int numLEDsActive(int mode, float sensorData, bool celsius) {
    int numLEDsActive;
    float minTemp = (celsius) ? TEMP_MIN_C : TEMP_MIN_F;
    float maxTemp = (celsius) ? TEMP_MAX_C : TEMP_MAX_F;
    float tempInterval = (celsius) ? LED_INTERVAL_TEMP_C : LED_INTERVAL_TEMP_F;
    switch (mode) {
        case 1:
            if (sensorData >= maxTemp)
                numLEDsActive = 14;
            else if (sensorData <= minTemp)
                numLEDsActive = 1;
            else
                numLEDsActive = int(round((sensorData - minTemp)/tempInterval));
            break;
        case 2:
            if (sensorData >= HUM_MAX)
                numLEDsActive = 14;
            else if (sensorData <= HUM_MIN)
                numLEDsActive = 1;
            else
                numLEDsActive = int(round((sensorData - HUM_MIN)/LED_INTERVAL_HUM));
            break;
        case 3:
            if (sensorData >= AIR_QUAL_MAX)
                numLEDsActive = 14;
            else if (sensorData <= AIR_QUAL_MIN)
                numLEDsActive = 1;
            else
                numLEDsActive = int(round((sensorData - AIR_QUAL_MIN)/LED_INTERVAL_AIR_QUAL));
            break;
        default:
            numLEDsActive = 0;
    }
    Serial.printf("NUMBER OF LEDS: %d\n", numLEDsActive);
    return numLEDsActive;
}

int getTempGoodMinLED(bool celsius) {
    float minGoodTemp = (celsius) ? TEMP_MIN_GOOD_C : TEMP_MIN_GOOD_F;
    float interval = (celsius) ? LED_INTERVAL_TEMP_C : LED_INTERVAL_TEMP_F;
    float minTemp = (celsius) ? TEMP_MIN_C : TEMP_MIN_F;
    Serial.printf("TEMP GOOD LED MIN: %d\n", int(round((minGoodTemp - minTemp) /interval)));
    return int(round((minGoodTemp - minTemp)/interval));
}

int getTempGoodMaxLED(bool celsius) {
    float maxGoodTemp = (celsius) ? TEMP_MAX_GOOD_C : TEMP_MAX_GOOD_F;
    float interval = (celsius) ? LED_INTERVAL_TEMP_C : LED_INTERVAL_TEMP_F;
    float minTemp = (celsius) ? TEMP_MIN_C : TEMP_MIN_F;
    Serial.printf("TEMP GOOD LED MAX: %d\n", int(round((maxGoodTemp - minTemp)/interval)));
    return int(round((maxGoodTemp - minTemp)/interval));
}

int getHumGoodMinLED() {
    return int(round((HUM_MIN_GOOD - HUM_MIN)/LED_INTERVAL_HUM));
}

int getHumGoodMaxLED() {
    return int(round((HUM_MAX - HUM_MAX_GOOD)/LED_INTERVAL_HUM));
}

int getAirQualGoodLED() {
    return int(round((AIR_QUAL_GOOD  - AIR_QUAL_MIN)/LED_INTERVAL_AIR_QUAL));
}

int getAirQualNotGoodLED() {
    return int(round((AIR_QUAL_NOT_GOOD - AIR_QUAL_MIN)/LED_INTERVAL_AIR_QUAL));
}