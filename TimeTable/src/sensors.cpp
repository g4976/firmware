#include "sensors.h"

#include <Arduino.h>
#include <SensirionI2CSht4x.h>
#include "SparkFun_SGP40_Arduino_Library.h"
#include <Wire.h>
#include "Firebase.h"

// SHT40
static SensirionI2CSht4x sht4x;
static bool celsius = false;
static float temperature = 20.0;
static float humidity = 50.0;

// SGP40
static SGP40 mySensor;
static int32_t vocIndex;

bool getFOrC() {
    return celsius;
}

void sensor_init()
{
    uint16_t error;
    char errorMessage[256];

    Wire.begin();

    // SHT40
    sht4x.begin(Wire);

    uint32_t serialNumber;
    error = sht4x.serialNumber(serialNumber);
    if (error) {
        Serial.print("Error trying to execute serialNumber(): ");
        errorToString(error, errorMessage, 256);
        Serial.println(errorMessage);
    } else {
        Serial.print("Serial Number: ");
        Serial.println(serialNumber);
    }

    // SGP40
    if (mySensor.begin() == false)
    {
        Serial.println(F("SGP40 not detected. Check connections."));
    }
}

void read_sensors()
{
    uint16_t error;
    char errorMessage[256];

    // SHT40
    float t = 0;
    float h = 0;

    for (uint16_t i = 0; i < NUM_T_H_MEASUREMENTS; i++)
    {
        error = sht4x.measureHighPrecision(temperature, humidity);
        if (error)
        {
            Serial.print("Error trying to execute measureHighPrecision(): ");
            errorToString(error, errorMessage, 256);
            Serial.println(errorMessage);
        }
        t = t + temperature;
        h = h + humidity;
    }
    
    temperature = t / NUM_T_H_MEASUREMENTS;
    if (!celsius)
    {
        temperature = (temperature * 1.8 + 32.0);
    }

    humidity = h / NUM_T_H_MEASUREMENTS;


    // SGP40
    vocIndex = mySensor.getVOCindex(humidity, temperature);
    Serial.println("VOC");
    Serial.println(vocIndex);

    // Update firebase
    String enviromentPath = getDeviceID() + "/environment";
    firebaseSetInt(vocIndex, enviromentPath + "/AQI");
    firebaseSetFloat(humidity, enviromentPath + "/humidity");
    firebaseSetFloat(temperature, enviromentPath + "/temperature");
}

float get_humidity()
{
    return humidity;
}

float get_temperature()
{
    return temperature;
}

int get_aqi()
{
    return vocIndex;
}
