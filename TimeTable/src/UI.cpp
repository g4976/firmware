#include "UI.h"
#include "LED.h"
#include "taskManager.h"
#include "modeManager.h"
#include "defines.h"

int counter = 0;
int currentStateCLK;
int lastStateCLK;
String currentDir ="";
unsigned long lastButtonPress = 0;
unsigned long lastModeButtonPress = 0;
unsigned long lastRotation = 0;

// void IRAM_ATTR encoderInterrupt() {
//     // If the DT state is different than the CLK state then
//     // the encoder is rotating CCW so decrement
//     if (digitalRead(DT) != currentStateCLK)
//     {
//         counter --;
//         currentDir ="CCW";
//         decrementSelectedTask();
//     } 
//     else 
//     {
//         // Encoder is rotating CW so increment
//         counter ++;
//         currentDir ="CW";
//         incrementSelectedTask();
//     }

// //    Serial.print("Direction: ");
// //    Serial.print(currentDir);
// //    Serial.print(" | Counter: ");
// //    Serial.println(counter);
// }

void UI_init()
{
    // Set encoder pins as inputs
    pinMode(CLK,INPUT);
    pinMode(DT,INPUT);
    pinMode(SW, INPUT_PULLUP);
    pinMode(BUTTON, INPUT_PULLUP);
    
    lastButtonPress = millis();
    lastRotation = millis();
    lastModeButtonPress = millis();
    // attachInterrupt(CLK, encoderInterrupt, RISING);

    // Read the initial state of CLK
//    lastStateCLK = digitalRead(CLK);
}

void readRotaryEncoder()
{   
    if (millis() - lastRotation > LED_TIMEOUT && getSelectedTask() != -1)
    {
        brightenSelectedLED(false);
        setSelectedTask(-1);
    }
    // Read the current state of CLK
    currentStateCLK = digitalRead(CLK);

    // If last and current state of CLK are different, then pulse occurred
    // React to only 1 state change to avoid double count
    if (currentStateCLK != lastStateCLK  && currentStateCLK == 1){

        if (getCurrentModeIndex() != 0) {
            turnOffLEDcolors();
            setModeDefault();
            setLEDcolors();
        }
        // If the DT state is different than the CLK state then
        // the encoder is rotating CCW so decrement
        if (digitalRead(DT) != currentStateCLK)
        {   
            counter --;
            // currentDir ="CCW";
            decrementSelectedTask();
            lastRotation = millis();
        } 
        else 
        {
            // Encoder is rotating CW so increment
            counter ++;
            // currentDir ="CW";
            incrementSelectedTask();
            lastRotation = millis();
        }

        // Serial.print("Direction: ");
        // Serial.print(currentDir);
        // Serial.print(" | Counter: ");
        // Serial.println(counter);
    }

    // Remember last CLK state
    lastStateCLK = currentStateCLK;

    // Read the button state
    int btnState = digitalRead(SW);

    //If we detect LOW signal, button is pressed
    if (btnState == LOW) {
        //if 50ms have passed since last LOW pulse, it means that the
        //button has been pressed, released and pressed again
        if (getCurrentModeIndex() != 0) {
            turnOffLEDcolors();
            setModeDefault();
            setLEDcolors();
        }
        
        if (millis() - lastButtonPress > 50) {
            Serial.println("Button pressed!");
            changeTaskState();
        }

        // Remember last button press event
        lastButtonPress = millis();
    }
}

void manageModeButton() {
    // Read the button state
    // Serial.println("Button pressed!");
    int modeBtnState = digitalRead(BUTTON);

    //If we detect LOW signal, button is pressed
    if (modeBtnState == LOW) {
        //if 50ms have passed since last LOW pulse, it means that the
        //button has been pressed, released and pressed again
        if (millis() - lastModeButtonPress > 50) {
            Serial.println("Button pressed!");
            changeModeState();
        }

        // Remember last button press event
        lastModeButtonPress = millis();
    }
}