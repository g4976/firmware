#include <Arduino.h>

#include "WiFiManager.h"
#include "Firebase.h"
#include "LED.h"
#include "UI.h"
#include "sensors.h"
#include "motion.h"
#include "ota.h"
#include "modeManager.h"

#include "defines.h"

#define UPDATE_TIME_SEND_DATA (60*1000)
#define UPDATE_TIME_READ_SENSORS (5*1000)
#define UPDATE_TIME_UI 1
#define MODE_DEFAULT_TIME (120*1000)

static unsigned long sendDataPrevMillis = 0;
static unsigned long readSensorsPrevMillis = 0;
static unsigned long modeStateMillis = 0;
static unsigned long uiPrevMS;

void setup() {
    pinMode(2, OUTPUT);
    pinMode(15,INPUT); //for resetting WiFi creds
    pinMode(36,INPUT); //for resetting WiFi creds
    setupEEPROM();
    Serial.begin(115200);
    if(!CheckWIFICreds()){
        Serial.println("No WIFI credentials stored in memory. Loading form...");
        digitalWrite(2,HIGH);
        while(loadWIFICredsForm());
    }
    else
    {
          if(!connectWiFi(getSSID(), getPW()))
          {
              wipeEEPROM();
              ESP.restart();
          }

        // Calibrate time to be local time
        const char* ntpServer = "pool.ntp.org";
        const long gmtOffset_sec = -18000;
        const int daylightOffset_sec = 3600;
        configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);

        // Run inits
        firebaseSetup();
        UI_init();
        LED_init();
        sensor_init();
        motion_init();

        digitalWrite(2,LOW);
    }
}

void loop() {
  
    // if(digitalRead(36) == LOW){
    //     Serial.println("Wiping WiFi credentials from memory...");
    //     wipeEEPROM();
    //     while(loadWIFICredsForm());
    // }
    if(firebaseReady() && (millis() - sendDataPrevMillis > UPDATE_TIME_SEND_DATA || sendDataPrevMillis == 0))
    {
        sendDataPrevMillis = millis();
        // firebaseSetFloat(0.01 + random(0,100), "test/float");
        // firebaseSetInt(42069, "test/int");
        // firebaseSetString("test", "test/string");
        firebaseQuery();
    }
    
    if((millis() - readSensorsPrevMillis > UPDATE_TIME_READ_SENSORS || readSensorsPrevMillis == 0))
    {
        readSensorsPrevMillis = millis();
        read_sensors();
        Serial.println("hum, temp, aqi");
        Serial.println(get_humidity());
        Serial.println(get_temperature());
        Serial.println(get_aqi());
        float sensorData;
        int modeIndex = getCurrentModeIndex();
        if (modeIndex == 1) {
            sensorData = get_temperature();
            setModeLEDcolors(sensorData, getFOrC());
        } else if (modeIndex == 2) {
            sensorData = get_humidity();
            setModeLEDcolors(sensorData, getFOrC());
        } else if (modeIndex == 3) {
            sensorData = float(get_aqi());
            setModeLEDcolors(sensorData, getFOrC());
        }

        checkSitTime();
        pollOTA();

        Serial.println("TEST OTA");
    }

    if((millis() - modeStateMillis > MODE_DEFAULT_TIME || modeStateMillis == 0))
    {
        modeStateMillis = millis();
        setModeDefault();
        turnOffLEDcolors();
        Serial.print("CURRENT MODE INDEX: ");
        Serial.println(getCurrentModeIndex());
        setLEDcolors();
    }

    readRotaryEncoder();
    manageModeButton();
}
