#include "ota.h"
#include "ArduinoJson.h"
#include "WiFi.h"
#include "HTTPClient.h"
#include "defines.h"
#include "Update.h"

// based off of https://medium.com/@mdwdotla/over-the-air-arduino-firmware-updates-using-firebase-part-2-1438139e4540

// Allocate a 1024-byte buffer for the JSON document.
StaticJsonDocument<1024> jsonDoc;

HTTPClient http;

// OTA
const char FIRMWARE_VERSION[] = ("__MaGiC__ " __DATE__ " " __TIME__ "__");

void pollOTA() {
    String url = "https://timetable-cbbd9-default-rtdb.firebaseio.com/config/" + WiFi.macAddress() + ".json";
    http.setTimeout(1000);
    http.begin(url);
    int status = http.GET();
    if (status <= 0) 
    {
        Serial.printf("HTTP error: %s\n", 
        http.errorToString(status).c_str());
        return;
    }
    String payload = http.getString();
    DeserializationError err = deserializeJson(jsonDoc, payload);      
    JsonObject jobj = jsonDoc.as<JsonObject>();
    const char *nextVersion = (const char *)jobj["version"];
    Serial.printf("NEXT VERSION: %s\n", nextVersion);
    Serial.printf("CURRENT VERSION: %s\n", FIRMWARE_VERSION);
    if (strcmp(nextVersion, FIRMWARE_VERSION) &&          
        strcmp(nextVersion, "none") &&        
        strcmp(nextVersion, "") &&        
        strcmp(nextVersion, "current"))
    {
        Serial.printf("String Compare nextVersion and firmwareVersion: %d\n", strcmp(nextVersion, FIRMWARE_VERSION));
        Serial.printf("String Compare nextVersion and none: %d\n", strcmp(nextVersion, "none"));
        Serial.printf("String Compare nextVersion and '': %d\n", strcmp(nextVersion, ""));
        Serial.printf("String Compare nextVersion and current: %d\n", strcmp(nextVersion, "current"));
        startFirmwareUpdate(nextVersion);
    }
}

void startFirmwareUpdate(String firmwareVersion) {  
    String url = "https://timetable-cbbd9-default-rtdb.firebaseio.com/firmware/" +
        firmwareVersion + ".json";
    http.setTimeout(1000);
    http.begin(url);
    int status = http.GET();
    if (status <= 0) {
        Serial.printf("HTTP error: %s\n", 
            http.errorToString(status).c_str());
        return;
    }
    String payload = http.getString();
    DeserializationError err = deserializeJson(jsonDoc, payload);      
    JsonObject jobj = jsonDoc.as<JsonObject>();
    String firmwareUrl = (const String &)jobj["url"];
    http.end();
    updateFirmware(firmwareUrl);
}

void updateFirmware(String firmwareUrl) {
    http.begin(firmwareUrl);
    int httpCode = http.GET();
    if (httpCode <= 0) {
        Serial.printf("HTTP failed, error: %s\n", 
        http.errorToString(httpCode).c_str());
        return;
    }
    // Check that we have enough space for the new binary.
    int contentLen = http.getSize();
    Serial.printf("Content-Length: %d\n", contentLen);
    bool canBegin = Update.begin(contentLen);
    if (!canBegin) {
        Serial.println("Not enough space to begin OTA");
        return;
    }
    // Write the HTTP stream to the Update library.
    WiFiClient* client = http.getStreamPtr();
    size_t written = Update.writeStream(*client);
    Serial.printf("OTA: %d/%d bytes written.\n", written, contentLen);
    if (written != contentLen) {
        Serial.println("Wrote partial binary. Giving up.");
        return;
    }
    if (!Update.end()) {
        Serial.println("Error from Update.end(): " + 
        String(Update.getError()));
        return;
    }
    if (Update.isFinished()) {
        Serial.println("Update successfully completed. Rebooting."); 
        // This line is specific to the ESP32 platform:
        ESP.restart();
    } else {
        Serial.println("Error from Update.isFinished(): " + 
        String(Update.getError()));
        return;
    }
}
