#include "LED.h"

#include <Arduino.h>
#include <FastLED.h>
#include "FastLED_RGBW.h"
#include "taskManager.h"
#include "modeManager.h"
#include "sensors.h"

static CRGBW leds[NUM_LEDS];
static CRGB *ledsRGB = (CRGB *) &leds[0];

int taskNumberToIndex(int taskNumber)
{
    return (MAX_TASKS-taskNumber+NUM_STATUS_LEDS-1);
}

int ledToIndexSensor(int led) {
    return NUM_STATUS_LEDS + led;
}

void LED_init()
{
    FastLED.addLeds<SK6812, LED_PIN, RGB>(ledsRGB, getRGBWsize(NUM_LEDS));
}

void updateTaskStateColor(int taskNumber, bool selected, bool update)
{
    int v = selected ? BRIGHT : DIM; //Make selected task brighter
    switch(getTaskStatus(taskNumber))
    {
        case 2:
            leds[taskNumberToIndex(taskNumber)] = CHSV(88, 255, v);
            break;
        case 1:
            leds[taskNumberToIndex(taskNumber)] = CHSV(35, 255, v);
            break;
        case 0:
            leds[taskNumberToIndex(taskNumber)] = CHSV(0, 255, v);
            break;
        default:
            // TODO: check if there's function to turn pixels off
            leds[taskNumberToIndex(taskNumber)] = CHSV(0,0,0);
            Serial.println("default");
    }
    if (update)
    {
        FastLED.show();
    }
}

void turnOffTaskLED(int taskNumber)
{
    // TODO: check if there's function to turn pixels off
    leds[taskNumberToIndex(taskNumber)] = CHSV(0,0,0);
    Serial.println("Turn Off Leds");
    FastLED.show();
}

void updateModeStateColor(int mode) {
    switch(mode)
    {   
        case 1:
            leds[0] = CHSV(204, 81, BRIGHT);
            leds[1] = CHSV(0,0,0);
            leds[2] = CHSV(0,0,0);
            break;
        case 2:
            leds[1] = CHSV(204, 81, BRIGHT);
            leds[2] = CHSV(0,0,0);
            leds[0] = CHSV(0,0,0);
            break;
        case 3:
            leds[2] = CHSV(204, 81, BRIGHT);
            leds[1] = CHSV(0,0,0);
            leds[0] = CHSV(0,0,0);
            break;
        default:
            // TODO: check if there's function to turn pixels off
            leds[0] = CHSV(0,0,0);
            leds[1] = CHSV(0,0,0);
            leds[2] = CHSV(0,0,0);
            setLEDcolors();
            Serial.println("Turn off all Mode LEDs");
    }
    FastLED.show();
}

void updateTempColors(int led, int goodLedMin, int goodLedMax) {
    CHSV color;
    for (int i = 0; i <= led; i++) {
        if (i < goodLedMin)
            color = CHSV(226, 186, BRIGHT);
        else if (i > goodLedMax)
            color = CHSV(8, 255, BRIGHT);
        else
            color = CHSV(110, 186, BRIGHT);
        leds[ledToIndexSensor(i)] = color;
    }

    Serial.printf("LED IS: %d. MAX TASKS is: %d\n", led, MAX_TASKS);
    // for (int i = led + 1; i < MAX_TASKS; i++) {
    //     leds[ledToIndexSensor(i)] = CHSV(0,0,0);
    // }
}

void updateHumColors(int led, int goodLedMin, int goodLedMax) {
    CHSV color;
    for (int i = 0; i <= led; i++) {
        if (i < goodLedMin)
            color = CHSV(8, 255, BRIGHT);
        else if (i > goodLedMax)
            color = CHSV(226, 186, BRIGHT);
        else
            color = CHSV(110, 186, BRIGHT);
        leds[ledToIndexSensor(i)] = color;
    }
    // for (int i = led + 1; i < MAX_TASKS; i++) {
    //     leds[ledToIndexSensor(i)] = CHSV(0,0,0);
    // }
}

void updateAirQualColors(int led, int goodLed, int notGoodLed) {
    CHSV color;
    for (int i = 0; i <= led; i++) {
        if (i <= goodLed)
            color = CHSV(110, 186, BRIGHT);
        else if (i > goodLed && i <= notGoodLed)
            color = CHSV(48, 234, BRIGHT);
        else
            color = CHSV(8, 255, BRIGHT);
        leds[ledToIndexSensor(i)] = color;
    }
    // for (int i = led + 1; i < MAX_TASKS; i++) {
    //     leds[ledToIndexSensor(i)] = CHSV(0,0,0);
    // }
}
// todo: add 3 bottom LEDs
void setLEDcolors()
{   
    for (int i = 0; i < getTaskNumber(); i++)
    {
        bool selected = (getSelectedTask() == i);
        updateTaskStateColor(i, selected, false);
    }
    FastLED.show();
}

void turnOffLEDcolors()
{   
    for (int i = 0; i < MAX_TASKS; i++)
    {
        turnOffTaskLED(i);
    }
    FastLED.show();
}

void brightenSelectedLED(bool bright)
{
    updateTaskStateColor(getSelectedTask(), bright, true);
}

void setModeLEDcolors(float sensorData, bool celsius) {
    int currActiveMode = getCurrentModeIndex();
    int numLedsActive = numLEDsActive(currActiveMode, sensorData, celsius);
    int goodLedMin;
    int goodLedMax;
    Serial.print("CURRENT ACTIVE MODE: ");
    Serial.println(currActiveMode);
    // updateModeStateColor(currActiveMode);
    switch (currActiveMode) {
        case 0:
            setLEDcolors();
            break;
        case 1:
            goodLedMin = getTempGoodMinLED(celsius);
            goodLedMax = getTempGoodMaxLED(celsius);
            updateTempColors(numLedsActive, goodLedMin, goodLedMax);
            break;
        case 2:
            goodLedMin = getHumGoodMinLED();
            goodLedMax = getHumGoodMaxLED();
            updateTempColors(numLedsActive, goodLedMin, goodLedMax);
            break;
        case 3:
            goodLedMin = getAirQualGoodLED();
            goodLedMax = getAirQualNotGoodLED();
            updateAirQualColors(numLedsActive, goodLedMin, goodLedMax);
            break;
        default:
            setLEDcolors();
    }
    FastLED.show();
}

void sitTimeWarning()
{
    // do stuff
}
