#include "taskManager.h"

#include "UI.h"
#include "LED.h"
#include "Firebase.h"
#include "defines.h"

struct Task {
    int active = 0, taskNum, taskStatus = 0;
    time_t dueTimeStamp; 
    String taskTitle, dueDate, dueDateFull;
};

struct Task tasks[MAX_TASKS];
int taskCount = 0;
int selectedTask = -1;
// int taskStatus[MAX_TASKS];
// String tasks[MAX_TASKS];

void setTaskCount(int ct)
{
    if (ct < MAX_TASKS)
    {
        taskCount = ct;
    }
}

void setTaskStatus(int i, int status)
{
    if (i >= 0 && i < (MAX_TASKS-1))
    {
        // switch(status)
        // {
        //     case 0:
        //         taskStatus[i] = TASK_STATUS_NOT_STARTED;
        //     case 1:
        //         taskStatus[i] = TASK_STATUS_IN_PROGRESS;
        //     case 2:
        //         taskStatus[i] = TASK_STATUS_COMPLETED;
        //     default:
        //         taskStatus[i] = TASK_STATUS_COUNT;
        // } 
        tasks[i].taskStatus = status;
    }
}

void setTask(int i, String task)
{
    if (i <= (MAX_TASKS-1))
    {
        tasks[i].taskTitle = task;
    }
}

void setTaskNum(int i, int taskNum){
    if (i <= (MAX_TASKS-1))
    {
        tasks[i].taskNum = taskNum;
    }
}

void setTaskActive(int i, int active){
    if (i <= (MAX_TASKS-1))
    {
        tasks[i].active = active;
    }
}

void setDueDateFull(int i, String dueDate)
{
    if (i <= (MAX_TASKS-1))
    {
        tasks[i].dueDateFull = dueDate;
    }
}

void setDueDate(int i, String dueDate)
{
    if (i <= (MAX_TASKS-1))
    {
        tasks[i].dueDate = dueDate;
    }
}

void setDueTimeStamp(int i, time_t timestamp)
{
    if (i <= (MAX_TASKS-1))
    {
        tasks[i].dueTimeStamp = timestamp;
    }
}

int getTaskStatus(int i)
{
    return tasks[i].taskStatus;
}


time_t getTaskTimeTillCompletion(int i) { 
    time_t curr_time;
     // Stores time seconds
    time(&curr_time);
    // printf("Seconds since January 1, 1970 = %ld\n", curr_time);
    // Serial.printf("CURRENT TIME: %ld \n", curr_time);
    return tasks[i].dueTimeStamp - curr_time;
}

int getTaskNumber()
{
    return taskCount;
}

int getTaskActive(int i){
        return tasks[i].active;
}

String getDueDateFull(int i){
        return tasks[i].dueDateFull;
}

String getDueDate(int i){
        return tasks[i].dueDate;
}

time_t getdueTimeStamp(int i){
        return tasks[i].dueTimeStamp;
}

String getTaskTitle(int i){
        return tasks[i].taskTitle;
}

int getTaskNum(int i){
        // What Task is this task
        // Kind of useless but there for completness
        return tasks[i].taskNum;
}


void decrementSelectedTask()
{
    updateTaskStateColor(selectedTask, false, true);
    selectedTask = (selectedTask == 0) ? taskCount-1 : (selectedTask - 1);
    updateTaskStateColor(selectedTask, true, true);
}

void incrementSelectedTask()
{
    updateTaskStateColor(selectedTask, false, true);
    selectedTask = (selectedTask + 1) >= taskCount ? 0 : (selectedTask + 1);
    updateTaskStateColor(selectedTask, true, true);
}

int getSelectedTask()
{
    return selectedTask;
}

void changeTaskState()
{
    tasks[selectedTask].taskStatus = (tasks[selectedTask].taskStatus == 2) ? 2 : (tasks[selectedTask].taskStatus + 1);
    firebaseChangeTaskState(selectedTask, tasks[selectedTask].taskStatus);
    updateTaskStateColor(selectedTask, true, true);
}

void setSelectedTask(int task)
{
    selectedTask = task;
}

void printTask(int i) {
    if (i <= (MAX_TASKS-1)) {
        Serial.printf("ACTIVE: %d, TASK TITLE: %s, TASK NUMBER: %d, TASK STATUS: %d, DUE TIMESTAMP: %ld, DUE DATE: %s, DUE DATE FULL: %s",
            tasks[i].active, tasks[i].taskTitle.c_str(), tasks[i].taskNum, tasks[i].taskStatus, tasks[i].dueTimeStamp, tasks[i].dueDate.c_str(), tasks[i].dueDateFull.c_str()
        );
    }
}