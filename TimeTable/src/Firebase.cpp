// BASED OFF OF https://randomnerdtutorials.com/esp32-firebase-realtime-database/

#include "Firebase.h"

#include "LED.h"
#include "taskManager.h"
#include "defines.h"
#include "einkDriver.h"
#include <WiFi.h>

#include <Firebase_ESP_Client.h>
// #include <chrono>
#include <sstream>

//Provide the token generation process info.
#include "addons/TokenHelper.h"
//Provide the RTDB payload printing info and other helper functions.
#include "addons/RTDBHelper.h"

static String deviceIDTemp = "/";
static const String deviceID = deviceIDTemp + DEVICE_ID;
    
//Define Firebase Data object
FirebaseData fbdo;
FirebaseJson tasksJson;
FirebaseAuth auth;
FirebaseConfig config;

static bool signupOK = false;

static int taskNumber = 0;

String getDeviceID()
{
  return deviceID;
}

bool firebaseReady()
{
    return (Firebase.ready() && signupOK);
}

void firebaseSetFloat(float f, String loc)
{
    if (Firebase.RTDB.setFloat(&fbdo, loc, f)){
      Serial.println("PASSED");
      Serial.println("PATH: " + fbdo.dataPath());
      Serial.println("TYPE: " + fbdo.dataType());
    }
    else {
      Serial.println("FAILED");
      Serial.println("REASON: " + fbdo.errorReason());
    }
}

void firebaseSetInt(int i, String loc)
{
    if (Firebase.RTDB.setInt(&fbdo, loc, i)){
      Serial.println("PASSED");
      Serial.println("PATH: " + fbdo.dataPath());
      Serial.println("TYPE: " + fbdo.dataType());
    }
    else {
      Serial.println("FAILED");
      Serial.println("REASON: " + fbdo.errorReason());
    }
}

void firebaseSetString(String s, String loc)
{
    if (Firebase.RTDB.setString(&fbdo, loc, s)){
      Serial.println("PASSED");
      Serial.println("PATH: " + fbdo.dataPath());
      Serial.println("TYPE: " + fbdo.dataType());
    }
    else {
      Serial.println("FAILED");
      Serial.println("REASON: " + fbdo.errorReason());
    }
}

void firebaseSetBool(bool b, String loc)
{
    if (Firebase.RTDB.setBool(&fbdo, loc, b)){
      Serial.println("PASSED");
      Serial.println("PATH: " + fbdo.dataPath());
      Serial.println("TYPE: " + fbdo.dataType());
    }
    else {
      Serial.println("FAILED");
      Serial.println("REASON: " + fbdo.errorReason());
    }
}

// USED FOR TESTING
void firebaseAddTask(String task)
{
    Firebase.RTDB.getInt(&fbdo, (deviceID + "/taskCount"));
    taskNumber = fbdo.intData();

    String taskLocation = deviceID + "/task" + taskNumber;

    firebaseSetString(task, taskLocation + "/taskName");
    firebaseSetInt(0, taskLocation + "/taskStatus");
    firebaseSetInt(++taskNumber, deviceID + "/taskCount");
}

void firebaseChangeTaskState(int taskNumber, int taskStatus)
{
    String taskLocation = deviceID + "/task" + taskNumber;
    firebaseSetInt(taskStatus, taskLocation + "/taskStatus");
}

void firebaseInitDevice()
{
    // Device location string
    Serial.println("deviceID");
    Serial.println((deviceID + "/update"));
    firebaseSetBool(true, (deviceID + "/update"));
    Firebase.RTDB.getBool(&fbdo, (deviceID + "/registered"));
    Serial.println("booldata");
    Serial.println(fbdo.boolData());

    if (fbdo.boolData() == false)
    {
        Serial.println(fbdo.boolData());
        firebaseSetBool(true, (deviceID + "/registered"));
        firebaseSetInt(0, (deviceID + "/taskCount"));
    }
}

void firebaseSetup()
{
    /* Assign the api key (required) */
    config.api_key = API_KEY;

    /* Assign the RTDB URL (required) */
    config.database_url = DATABASE_URL;


    auth.user.email = USER_EMAIL;
    auth.user.password = USER_PASSWORD;

    // /* Sign up */
    // if (Firebase.signUp(&config, &auth, USER_EMAIL, USER_PASSWORD)){
    //     Serial.println("ok");
    //     signupOK = true;
    // }
    // else{
    //     Serial.printf("%s\n", config.signer.signupError.message.c_str());
    // }
    signupOK = true;
    /* Assign the callback function for the long running token generation task */
    config.token_status_callback = tokenStatusCallback; //see addons/TokenHelper.h
    
    Firebase.begin(&config, &auth);
    Firebase.reconnectWiFi(true);

    Serial.println("start init");
    firebaseInitDevice();
    

    // FOR TESTING
    // firebaseAddTask("test");
}

// TODO: Save Tasks to eeprom
void firebaseQuery()
{
    if (signupOK)
    {
        Firebase.RTDB.getBool(&fbdo, (deviceID + "/update"));
        if (fbdo.boolData())
        {
//             Firebase.RTDB.getInt(&fbdo, (deviceID + "/taskCount"));
//             taskNumber = fbdo.intData();

//             taskNumber = taskNumber < 14 ? taskNumber : 14;
//             setTaskCount(taskNumber);

//             for (int i = 0; i < taskNumber; i++)
//             {
//                 String taskLocation = deviceID + "/task" + i;

//                 Firebase.RTDB.getString(&fbdo, (taskLocation + "/taskName"));
//                 String task = fbdo.stringData();
//                 setTask(i, task);

//                 Firebase.RTDB.getInt(&fbdo, (taskLocation + "/taskStatus"));
//                 int status = fbdo.intData();
//                 setTaskStatus(i, status);
//             }
//             firebaseSetBool(false, (deviceID + "/update"));
// //            updateEink();
//             setLEDcolors();
			Firebase.RTDB.getJSON(&fbdo, (deviceID + "/tasks"), &tasksJson);
			tasksJson = fbdo.jsonObject();
			size_t len = tasksJson.iteratorBegin();
			// Serial.printf("LENGTH of JSON IS: %d", len);
			// tasksJson.toString(Serial, true);
			// Serial.println();
			// Serial.println();
			FirebaseJson::IteratorValue value;
			for (size_t i = 0; i < len; i++)
			{	
				value = tasksJson.valueAt(i);
				if (value.key == "title") {
					String title = value.value;
          title.remove(title.length() - 1);
          title = title.substring(1);
					// Serial.printf("TITLE: %s\n", title.c_str());
					// Serial.printf("TASK NUMBER: %d\n", taskNumber);
					setTask(taskNumber, title);
					setTaskNum(taskNumber, taskNumber);
					setTaskActive(taskNumber, 1);
					printTask(taskNumber);
					taskNumber++;
				}
				if (value.key == "due") {
					String due = value.value;
					// Serial.printf("TASKNUMBER: %d\n", taskNumber);
					
					setDueDateFull(taskNumber, due);
				}
				if (value.key == "dueDate") {
					String dueDate = value.value;
					setDueDate(taskNumber, dueDate);
					// newTask.dueTime = t;
				}
				if (value.key == "dueTime") {
					String dueTime = value.value;
					// Serial.printf("DUE DATE TIME: %s \n", dueTime.c_str());
					dueTime.remove(dueTime.length() - 3);
					// dueTime.remove(dueTime.length() - 1);
					// dueTime.remove(dueTime.length() - 1);
					time_t unix_timestamp = atol(dueTime.c_str());
					setDueTimeStamp(taskNumber, unix_timestamp);
				}
				
				if (value.key == "status") {
					int status = value.value.toInt();
					setTaskStatus(taskNumber, status);
				}

				if (taskNumber == MAX_TASKS)
					break;
			}
			tasksJson.iteratorEnd();
			setTaskCount(taskNumber);
			// Serial.printf("NUMBER OF TASKS: %d", taskNumber);
			taskNumber = 0;
			updateEink();
			// setLEDcolors();
		}
    }
}
