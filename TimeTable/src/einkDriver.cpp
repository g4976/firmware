#include "einkDriver.h"
#include "taskManager.h"
#include "custom_bitmaps\taskFormatImage.h"

// base class GxEPD2_GFX can be used to pass references or pointers to the display instance as parameter, uses ~1.2k more code
// enable or disable GxEPD2_GFX base class
#define ENABLE_GxEPD2_GFX 0
#include <GxEPD2_BW.h>
#include <Arduino.h>
#include <Wire.h>
#include <Adafruit_I2CDevice.h>
#include <Adafruit_GFX.h>
#include <Fonts/FreeSansBold9pt7b.h>

// select the display constructor line in one of the following files (old style):
#include "GxEPD2_display_selection.h"

#if !defined(__AVR) && !defined(_BOARD_GENERIC_STM32F103C_H_) && !defined(ARDUINO_BLUEPILL_F103C8)
// #include "bitmaps/Bitmaps800x480.h" // 7.5"  b/w
#endif


#if defined(ARDUINO_ARCH_RP2040) && defined(ARDUINO_RASPBERRY_PI_PICO)
// SPI pins used by GoodDisplay DESPI-PICO. note: steals standard I2C pins PIN_WIRE_SDA (6), PIN_WIRE_SCL (7)
// uncomment next line for use with GoodDisplay DESPI-PICO.
arduino::MbedSPI SPI0(4, 7, 6); // need be valid pins for same SPI channel, else fails blinking 4 long 4 short
#endif
// TODO: FIGURE OUT EINK STUFF

// void setup_eink()
// {
//     Serial.begin(115200);
//     Serial.println();
//     Serial.println("SETUP EINK");
//     for (int i = 0; i < 14; i++){
//         std::stringstream ss;
//         ss << i;
//         std::string str;
//         ss >> str;
//         std::string taskName = "Task " + str;
//         setTask(i, taskName.c_str());
//         setTaskStatus(i, 0);
//     }
//     setTaskCount(14);
//     int numTasks = getTaskNumber();
//     std::stringstream ss;
//     ss << numTasks;
//     std::string str;
//     ss >> str;
//     std::string cnt = "Number of Tasks: " + str;
//     Serial.println(cnt.c_str());
// }

void updateEink() {
    Serial.println("UPDATE EINK");
    delay(100);
    // display.init(115200, true, 2, false); // USE THIS for Waveshare boards with "clever" reset circuit, 2ms reset pulse
    int numTasks = getTaskNumber();
    // struct Task tasks[14];
    // for (int i = 0; i < numTasks; i++){
    //     tasks[i].taskTitle = getTask(i);
    //     tasks[i].dueDate = "Today";
    //     tasks[i].taskStatus = getTaskStatus(i);
    //     tasks[i].active = 1;
    //     Serial.println(tasks[i].taskTitle);
    //     Serial.println(tasks[i].dueDate);
    //     std::stringstream ss;
    //     ss << i;
    //     std::string str;
    //     ss >> str;
    //     std::string t = "Task " + str + " active";
    //     if (tasks[i].active == 1){
    //         Serial.println(t.c_str());
    //     }
    // }

    // delay(1000);
    // display.setRotation(2);
    // drawBitmap();
    display.powerOff();
    delay(1000);
    display.init(115200, true, 2, false);
    display.setRotation(3);
    display.setFont(&FreeSansBold9pt7b);
    if (display.epd2.WIDTH < 104) display.setFont(0);
    display.setTextColor(GxEPD_BLACK);
    int16_t tbx, tby; uint16_t tbw, tbh;
    uint16_t x[14];
    uint16_t y[14];
    int height_offset = 90;
    // uint16_t x_date[14];
    // uint16_t y_date[14];
    // uint16_t x_status[14];
    // uint16_t y_status[14];
    // String status;
    for (int i = 0; i < numTasks; i++){
        // get task title boundary
        display.getTextBounds(getTaskTitle(i), 0, 0, &tbx, &tby, &tbw, &tbh);
        // position bounding box by transposition of origin:
        x[i] = ((display.width() - tbw) / 14) - tbx;
        y[i] = ((i+1) * ((display.height() - tbh - height_offset) / 14)) - tby;

        // // get task due date boundary
        // display.getTextBounds(tasks[i].dueDate, 0, 0, &tbx, &tby, &tbw, &tbh);
        // x_date[i] = (6 * ((display.width() - tbw) / 8)) - tbx;
        // y_date[i] = ((i+1) * ((display.height() - tbh) / 14)) - tby;

        // // get task status boundary
        // switch(tasks[i].taskStatus) {
        //     case 0:
        //         status = "To Do";
        //         break;

        //     case 1:
        //         status = "In Progress";
        //         break;
            
        //     case 2:
        //         status = "Completed!";
        //         break;
            
        //     default:
        //         status = "To Do";
        // }

        // display.getTextBounds(status, 0, 0, &tbx, &tby, &tbw, &tbh);
        // x_status[i] = (8 * ((display.width() - tbw) / 8)) - tbx;
        // y_status[i] = ((i+1) * ((display.height() - tbh) / 15)) - tby;
    }
    display.setFullWindow();
    display.firstPage();
    do
    {
        
        display.fillScreen(GxEPD_WHITE);
        display.drawInvertedBitmap(0, 0, epd_bitmap_allArray[0], 480, 800, GxEPD_BLACK);
        
        for (int i = 0; i < numTasks; i++) {
            if (getTaskActive(i) == 1) {
                display.setCursor(x[i], y[i]);
                display.print(getTaskTitle(i));
                // display.setCursor(x_date[i], y_date[i]);
                // display.print(tasks[i].dueDate);
                // display.setCursor(x_status[i], y_status[i]);
                // display.print(status);
            }
        }
    }
    while (display.nextPage());
    display.powerOff();
}
// #ifdef _GxBitmaps800x480Task_H_
// void drawBitmap() {
//     if ((display.epd2.WIDTH == 800) && (display.epd2.HEIGHT == 480))
//     {
//         for (uint16_t i = 0; i < sizeof(epd_bitmap_allArray) / sizeof(char*); i++)
//         {
//         display.firstPage();
//         do
//         {
//             display.fillScreen(GxEPD_WHITE);
//             display.drawInvertedBitmap(0, 0, epd_bitmap_allArray[i], 480, 800, GxEPD_BLACK);
//         }
//         while (display.nextPage());
//         delay(2000);
//         }
//         if (display.epd2.panel == GxEPD2::GDEW075T7)
//         {
//         // avoid ghosting caused by OTP waveform
//         display.clearScreen();
//         display.refresh(false); // full update
//         }
//     }
// }
// #endif


// void displayTasks(struct Task tasks[14], int numTasks)
// {
//   //Serial.println("helloWorld");
//     display.setRotation(1);
//     display.setFont(&FreeMonoBold9pt7b);
//     if (display.epd2.WIDTH < 104) display.setFont(0);
//     display.setTextColor(GxEPD_BLACK);
//     int16_t tbx, tby; uint16_t tbw, tbh;
//     uint16_t x[14];
//     uint16_t y[14];
//     uint16_t x_date[14];
//     uint16_t y_date[14];
//     uint16_t x_status[14];
//     uint16_t y_status[14];
//     std::string status;
//     for (int i = 0; i < numTasks; i++){
//         // get task title boundary
//         display.getTextBounds(tasks[i].taskTitle, 0, 0, &tbx, &tby, &tbw, &tbh);
//         // position bounding box by transposition of origin:
//         x[i] = ((display.width() - tbw) / 8) - tbx;
//         y[i] = ((i+1) * ((display.height() - tbh) / 14)) - tby;

//         // get task due date boundary
//         display.getTextBounds(tasks[i].dueDate, 0, 0, &tbx, &tby, &tbw, &tbh);
//         x_date[i] = (6 * ((display.width() - tbw) / 8)) - tbx;
//         y_date[i] = ((i+1) * ((display.height() - tbh) / 14)) - tby;

//         // get task status boundary
//         switch(tasks[i].taskStatus) {
//             case 0:
//                 status = "To Do";
//                 break;

//             case 1:
//                 status = "In Progress";
//                 break;
            
//             case 2:
//                 status = "Completed!";
//                 break;
            
//             default:
//                 status = "To Do";
//         }

//         display.getTextBounds(status, 0, 0, &tbx, &tby, &tbw, &tbh);
//         x_status[i] = (7 * ((display.width() - tbw) / 8)) - tbx;
//         y_status[i] = ((i+1) * ((display.height() - tbh) / 14)) - tby;
//     }
//     display.setFullWindow();
//     display.firstPage();
//     do
//     {
//         display.fillScreen(GxEPD_WHITE);
//         for (int i = 0; i < numTasks; i++) {
//             if (tasks[i].active == 1) {
//                 display.setCursor(x[0], y[0]);
//                 display.print(tasks[0].taskTitle);
//                 display.setCursor(x_date[0], y_date[0]);
//                 display.print(tasks[0].dueDate);
//                 display.setCursor(x_status[0], y_status[0]);
//                 display.print(tasks[0].taskStatus);
//             }
//         }
//     }
//     while (display.nextPage());
//     //Serial.println("helloWorld done");
// }
